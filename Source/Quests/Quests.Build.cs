// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Quests : ModuleRules
{
	public Quests(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
